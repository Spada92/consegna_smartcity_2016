\select@language {italian}
\select@language {italian}
\contentsline {section}{\numberline {1}Smart City al giorno d'oggi}{6}{section.1}
\contentsline {section}{\numberline {2}Descrizione di AirQualityBox}{7}{section.2}
\contentsline {subsection}{\numberline {2.1}AirQualityBox nell'ambito Smart City}{7}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Esempio di utilizzo 1}{7}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Esempio di utilizzo 2}{8}{subsubsection.2.1.2}
\contentsline {section}{\numberline {3}Livelli di particolato limite per la legge}{9}{section.3}
\contentsline {section}{\numberline {4}Progettazione e sviluppo}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}Sviluppo script}{10}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Controller}{12}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Sensori}{13}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Storer}{14}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4}Misure effettuate e database utilizzati}{14}{subsubsection.4.1.4}
\contentsline {paragraph}{\numberline {4.1.4.1}Gestione database locale e online}{15}{paragraph.4.1.4.1}
\contentsline {subsubsection}{\numberline {4.1.5}Sender}{16}{subsubsection.4.1.5}
\contentsline {subsubsection}{\numberline {4.1.6}File \textit {config.json} e Configurator}{16}{subsubsection.4.1.6}
\contentsline {paragraph}{\numberline {4.1.6.1}Configurator}{18}{paragraph.4.1.6.1}
\contentsline {subsubsection}{\numberline {4.1.7}Qualche linea di codice}{18}{subsubsection.4.1.7}
\contentsline {paragraph}{\numberline {4.1.7.1}Codice relativo a container\_rain}{18}{paragraph.4.1.7.1}
\contentsline {paragraph}{\numberline {4.1.7.2}Procedura all'interno dello Storer}{19}{paragraph.4.1.7.2}
\contentsline {paragraph}{\numberline {4.1.7.3}Qualcosa del Sender}{19}{paragraph.4.1.7.3}
\contentsline {subsection}{\numberline {4.2}Sviluppo front-end}{19}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}L'applicazione}{20}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Analisi possibile scelta di sviluppo: app nativa}{22}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Analisi possibile scelta di sviluppo: web app}{22}{subsubsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.4}Analisi possibile scelta di sviluppo: app ibrida}{23}{subsubsection.4.2.4}
\contentsline {subsubsection}{\numberline {4.2.5}La scelta: app ibrida}{24}{subsubsection.4.2.5}
\contentsline {subsubsection}{\numberline {4.2.6}Progetto: parte front end e back end}{24}{subsubsection.4.2.6}
\contentsline {paragraph}{\numberline {4.2.6.1}Parte front end}{25}{paragraph.4.2.6.1}
\contentsline {paragraph}{\numberline {4.2.6.2}Parte back end}{25}{paragraph.4.2.6.2}
\contentsline {subsubsection}{\numberline {4.2.7}Alcuni accorgimenti}{26}{subsubsection.4.2.7}
\contentsline {subsubsection}{\numberline {4.2.8}Deploy su mobile e pc}{27}{subsubsection.4.2.8}
\contentsline {paragraph}{\numberline {4.2.8.1}App mobile}{27}{paragraph.4.2.8.1}
\contentsline {paragraph}{\numberline {4.2.8.2}App desktop}{27}{paragraph.4.2.8.2}
\contentsline {subsection}{\numberline {4.3}Sviluppo Configuratore}{28}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Dashboard}{28}{subsubsection.4.3.1}
\contentsline {paragraph}{\numberline {4.3.1.1}Funzionamento}{29}{paragraph.4.3.1.1}
\contentsline {subsubsection}{\numberline {4.3.2}Sensori}{30}{subsubsection.4.3.2}
\contentsline {paragraph}{\numberline {4.3.2.1}Funzionamento}{31}{paragraph.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.3}Impostazioni}{31}{subsubsection.4.3.3}
\contentsline {paragraph}{\numberline {4.3.3.1}Funzionamento}{32}{paragraph.4.3.3.1}
\contentsline {subsubsection}{\numberline {4.3.4}Struttura del front-end}{33}{subsubsection.4.3.4}
\contentsline {section}{\numberline {5}Test effettuati}{35}{section.5}
\contentsline {section}{\numberline {6}Strumenti usati e linguaggi}{36}{section.6}
\contentsline {subsection}{\numberline {6.1}Database online: \textit {openshift}}{36}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Linguaggio Python}{36}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Flask-restful}{37}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}Operazioni \textit {get} e \textit {post}}{37}{subsubsection.6.2.2}
\contentsline {subsection}{\numberline {6.3}RESTClient}{37}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Ionic Framework}{38}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}Electron framework}{38}{subsection.6.5}
\contentsline {subsection}{\numberline {6.6}MongoDb e breve comparativa tra database relazionali e NoSQL}{39}{subsection.6.6}
\contentsline {subsection}{\numberline {6.7}Robomongo}{40}{subsection.6.7}
\contentsline {subsection}{\numberline {6.8}JSON per la comunicazione}{41}{subsection.6.8}
\contentsline {subsubsection}{\numberline {6.8.1}BSON}{41}{subsubsection.6.8.1}
\contentsline {section}{\numberline {7}Problematiche riscontrate}{42}{section.7}
\contentsline {subsection}{\numberline {7.1}Campo `link' nel file di configurazione}{42}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Misura del particolato con Shinyei PPD42NS}{42}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Documentazione sensore MQ9}{42}{subsection.7.3}
\contentsline {subsection}{\numberline {7.4}Gestione date con MongoDB}{43}{subsection.7.4}
\contentsline {subsection}{\numberline {7.5}Ionic Framework e IOS}{44}{subsection.7.5}
\contentsline {section}{\numberline {8}Possibili sviluppi futuri}{45}{section.8}
\contentsline {subsection}{\numberline {8.1}Sicurezza}{45}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Altro}{45}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Miglioramento statistiche}{45}{subsection.8.3}
\contentsline {subsection}{\numberline {8.4}Supporto windows phone}{45}{subsection.8.4}
\contentsline {subsection}{\numberline {8.5}Sicurezza della comunicazione - File \textit {config.json}}{45}{subsection.8.5}
\contentsline {section}{\numberline {9}Conclusioni}{47}{section.9}
\contentsline {section}{\numberline {10}Riferimenti a documenti esterni}{47}{section.10}
\contentsline {section}{\numberline {A}Lista dei componenti utilizzati}{48}{appendix.Alph1}
\contentsline {subsection}{\numberline {A.1}Guida GrovePi+}{48}{subsection.Alph1.1}
\contentsline {subsubsection}{\numberline {A.1.1}Aggiornamento firmware}{50}{subsubsection.Alph1.1.1}
\contentsline {subsection}{\numberline {A.2}Sensori e il loro interfacciamento}{51}{subsection.Alph1.2}
\contentsline {subsubsection}{\numberline {A.2.1}Sensore di temperatura e di umidit\IeC {\`a}}{51}{subsubsection.Alph1.2.1}
\contentsline {subsubsection}{\numberline {A.2.2}Sensore di pioggia}{51}{subsubsection.Alph1.2.2}
\contentsline {subsubsection}{\numberline {A.2.3}Sensore di particolato (Dust)}{52}{subsubsection.Alph1.2.3}
\contentsline {subsubsection}{\numberline {A.2.4}Sensore di gas (MQ9)}{55}{subsubsection.Alph1.2.4}
\contentsline {subsubsection}{\numberline {A.2.5}Sensore di posizione (GPS)}{59}{subsubsection.Alph1.2.5}
\contentsline {subsection}{\numberline {A.3}Altri componenti}{60}{subsection.Alph1.3}
\contentsline {subsubsection}{\numberline {A.3.1}Ledbar}{60}{subsubsection.Alph1.3.1}
\contentsline {section}{\numberline {B}MongoDB}{62}{appendix.Alph2}
\contentsline {subsection}{\numberline {B.1}Identificatore di un documento}{62}{subsection.Alph2.1}
\contentsline {section}{\numberline {C}Ionic framework}{63}{appendix.Alph3}
\contentsline {subsection}{\numberline {C.1}Installazione framework}{63}{subsection.Alph3.1}
\contentsline {subsection}{\numberline {C.2}Configurazione del Raspberry per Ionic}{63}{subsection.Alph3.2}
\contentsline {subsection}{\numberline {C.3}Debug con browser}{64}{subsection.Alph3.3}
\contentsline {subsection}{\numberline {C.4}Prestazioni in dispositivi datati: Crosswalk}{65}{subsection.Alph3.4}
\contentsline {section}{\numberline {D}Electron framework}{67}{appendix.Alph4}
\contentsline {subsection}{\numberline {D.1}Electron packager}{67}{subsection.Alph4.1}
\contentsline {section}{\numberline {E}Librerie per applicativo front end}{68}{appendix.Alph5}
\contentsline {section}{\numberline {F}Google Maps Javascript API}{69}{appendix.Alph6}
\contentsline {section}{\numberline {G}Riferimenti esterni}{71}{appendix.Alph7}
