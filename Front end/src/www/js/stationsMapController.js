angular.module('starter')
//**********PER GOOGLE MAPS**************
.controller('stationsMap', function ($scope, $ionicLoading, $ionicPopup, stationsList) {

	//dati del server
	var data;
	//per la mappa
	var map;
	var myLatlng;
	var mapOptions;
	var markers=[];

	//per fare gli alert con grafica decente
	$scope.showAlert = function (title, text) {
		var alertPopup = $ionicPopup.alert({
				title : title,
				template : text
			});
	};

	//lista di liste di stazioni (ogni numero di stazione ha pi� dati)

	//setto gli onclick dei marker della mappa (prendi i dati dal server+prendo il numero di stazione pi� recente in base alle coordinate)
	function doJob() {

		//scarico i dati dal server
		stationsList.downloadLastStationsData().then(function (res) {
			data = res;
			//check errori
			if (res == 'err') {
				$scope.showAlert('Errore di rete', 'Impossibile raggiungere il server...controlla la tua connessione e riprova!');
			} else {
				//cesena (cos� se non va il gps punta l� la mappa)
				myLatlng = new google.maps.LatLng(44.1397615, 12.2410306);
				mapOptions = {
					center : myLatlng,
					zoom : 16,
					mapTypeId : google.maps.MapTypeId.ROADMAP
				};
				map = new google.maps.Map(document.getElementById("map"), mapOptions);

				//centro la posizione corrente nella mappa
				navigator.geolocation.getCurrentPosition(function (pos) {
					map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
				},
					function (error) {
					$scope.showAlert('GPS error', 'It seems that it\'s impossible to get the current position. Please check if gps is active and try again (NOTE: you can locate the stations anyway without GPS, but the map won\'t be centered to your current position)')
				}, {
					enableHighAccuracy : true,
					timeout : 5000
				});
				//metti i marker nella mappa con gli onclick per vederne i dati
				for (var i = 0; i < data.length; i++) {
					var num = data[i].StationNumber;
					var marker = new google.maps.Marker({
							position : new google.maps.LatLng(data[i].GPS_latitude, data[i].GPS_longitude),
							map : map,
							title : "Stazione " + num,
							url : num
						});
					markers.push(marker);
				}
				for (var i = 0; i < markers.length; i++) {
					new google.maps.event.addListener(markers[i], 'click', function () {
						window.location.href = "#/app/station" + "?" + this.url;
					});
				}
			}
		});

	}

	//FACCIO IL LAVORO DEL CONTROLLER
	doJob();

});
