angular.module('starter')

//**********PER I DATI DELLA STAZIONE**************
.controller('stationData', function ($scope, $location, $ionicLoading, $ionicPopup, stationsList) {

	//geolocalizzatore
	var geocoder;
	geocoder = new google.maps.Geocoder();
	var citymap;
	var city2map;
	var countrymap;
	var countryCodemap;
	var number = window.location.href.split("?")[1];

	var country;

	//dati del server
	var data;
	//item della lista
	$scope.items = []

	//cerco di prendere i dati relativi alla posizione
	function findCountryAndPushToList(lat, lng, lista) {
		var latlng = {
			lat : parseInt(lat),
			lng : parseInt(lng)
		};
		geocoder.geocode({
			'location' : latlng
		}, function (results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				//il GPS funziona
				if (results[1]) {
					//c'� una localit�
					country = results[1].formatted_address;
					lista.push("Localita': " + country);
					pushToList(lista);
				} else {
					//nessuna localit� trovata
					$scope.showAlert('Errore di localizzazione', 'Impossibile trovare la localita\' della stazione! Probabilmente la stazione si trova in un\'area non raggiungibile!');
					lista.push("Localita': " + "non disponibile");
					pushToList(lista);
				}
			} else {
				//il GPS non funziona
				$scope.showAlert('Errore di localizzazione', 'Impossibile trovare la localita\' della stazione! Probabilmente non hai abilitato il GPS sul tuo dispositivo, oppure non sono presenti i dati relativi alla latitudine e longitudine della stazione!');
				lista.push("Localita': " + "non disponibile");
				pushToList(lista);
			}
		});
	}

	function pushToList(lista) {
		//aggiungo gli elementi alla ion list
		for (var i = 0; i < lista.length; i++) {
			$scope.items.push(lista[i]);
		}
	}

	//per fare gli alert con grafica decente
	$scope.showAlert = function (title, text) {
		var alertPopup = $ionicPopup.alert({
				title : title,
				template : text
			});
	};

	//refresh dati
	$scope.doRefresh = function () {
		//pulisco la lista
		$scope.items = [];
		//scarico i dati
		stationsList.downloadLastStationsData(number).then(function (res) {
			data = res;
			if (res == 'err') {
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
				$scope.showAlert('Errore di rete', 'Impossibile raggiungere il server...controlla la tua connessione e riprova!');
			} else {
				doJob();
			}
		});
	};

	//prendo i dati del server + prendo le stazioni con il numero +prendo i dati pi� recenti
	function doJob() {
		//mostro lo spinner: inizio a caricare
		$ionicLoading.show({
			templateUrl : 'templates/spinner.html',
			scope : $scope
		});
		//scarico i dati dal server
		stationsList.downloadLastStationData(number).then(function (res) {
			data = res;
			//check errori
			if (res == 'err') {
				$ionicLoading.hide();
				//evento per il pull down refresh (indica che � terminato)
				$scope.$broadcast('scroll.refreshComplete');
				$scope.showAlert('Errore di rete', 'Impossibile raggiungere il server...controlla la tua connessione e riprova!');
			} else {
				//lista di dati della stazione
				var lista = [];
				//setto l'header della lista
				$scope.header = " Stazione " + data[0].StationNumber;
				var d = new Date(data[0].date);
				lista.push("Data: " + d.getUTCDate() + "/" + (d.getUTCMonth() + 1) + "/" + d.getUTCFullYear()+" Ora: "+(d.getUTCHours())+":"+(d.getUTCMinutes())+":"+d.getUTCSeconds());
				if (data[0].Humidity == "Error") {
					lista.push(" Umidita': " + "errore nella misurazione ");
				} else if (data[0].Humidity == "Disabled") {
					lista.push("Umidita': " + "sensore non attivo ");
				} else {
					lista.push("Umidita': " + data[0].Humidity + "\%");
				}
				if (data[0].Temperature == "Error") {
					lista.push("Temperatura: " + "errore nella misurazione ");
				} else if (data[0].Temperature == "Disabled") {
					lista.push("Temperatura: " + "sensore non attivo ");
				} else {
					lista.push("Temperatura: " + data[0].Temperature + " gradi centigradi");
				}
				var p = data[0].Rain;
				if (p == "Error") {
					lista.push("Pioggia: " + "errore nella misurazione ");
				} else if (p == " Disabled ") {
					lista.push("Pioggia: " + "sensore non attivo ");
				} else {
					if (p == 0) {
						p = "Non piove ";
						lista.push("Pioggia: " + p);
					} else if (p == 1) {
						p = "Piove ";
						lista.push("Pioggia: " + p);
					}
				}
				if (data[0].GPS_altitude == "Error") {
					lista.push("Altitudine: " + "errore nella misurazione ");
				} else if (data[0].GPS_altitude == "Disabled") {
					lista.push("Altitudine: " + "sensore non attivo ");
				} else {
					lista.push("Altitudine : " + data[0].GPS_altitude + " metri ");
				}
				if (data[0].Dust == "Error") {
					lista.push("Particolato: " + "errore nella misurazione ");
				} else if (data[0].Dust == "Disabled") {
					lista.push("Particolato: " + "sensore non attivo ");
				} else {
					lista.push("Particolato : " + data[0].Dust + " microgrammi per metro cubo ");
				}
				if (data[0].MQ9 == "Error") {
					lista.push("MQ9: " + "errore nella misurazione ");
				} else if (data[0].MQ9 == "Disabled") {
					lista.push("MQ9: " + "sensore non attivo ");
				} else {
					lista.push("MQ9: " + data[0].MQ9 + " parti per milione ");
				}
				if (data[0].GPS_latitude == "Error") {
					lista.push("Latitudine: " + "errore nella misurazione ");
				} else if (data[0].GPS_latitude == "Disabled") {
					lista.push("Latitudine: " + "sensore non attivo ");
				} else {
					lista.push("Latitudine: " + data[0].GPS_latitude);
				}
				if (data[0].GPS_longitude == "Error") {
					lista.push("Longitudine: " + "errore nella misurazione ");
				} else if (data[0].GPS_longitude == "Disabled") {
					lista.push("Longitudine: " + "sensore non attivo ");
				} else {
					lista.push("Longitudine: " + data[0].GPS_longitude);
				}
				//prendo la localit� e pusho i dati nella lista
				findCountryAndPushToList(data[0].GPS_latitude, data[0].GPS_longitude, lista);
				//onclick del footer per localizzare la stazione
				document.getElementById("locateMaps").onclick = function () {
					//basta un controllo tanto l'altro sar� disattivato di conseguenza
					if(data[0].GPS_longitude!="Error" && data[0].GPS_longitude!="Disabled"){
						window.location.href = "#/app/locatestation" + "?" + data[0].GPS_latitude + "&" + data[0].GPS_longitude;
					}	else{
						$scope.showAlert('Errore', 'Impossibile localizzare la stazione poiche\' mancano i dati relativi alla sua latitudine e longitudine!');
					}				
				}
				//evento per il pull down refresh (indica che � terminato)
				$scope.$broadcast('scroll.refreshComplete');
				//nascondo lo spinner: ho finito di caricare (solo quando viene lanciata la pagina la prima volta, non � del pull down refresh)
				$ionicLoading.hide();
			}
		});

	}

	//FACCIO IL LAVORO DEL CONTROLLER
	doJob();

});
