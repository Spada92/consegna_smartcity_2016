angular.module('starter')
//**********PER GOOGLE MAPS**************
.controller('locateStation', function ($scope, $ionicLoading) {

	//recupero coordinate da url
	var url = window.location.href.split("?")[1];
	var coords = url.split("&");
	var latit = coords[0];
	var longit = coords[1];
	var myLatlng = new google.maps.LatLng(latit, longit);

	//mappa google maps
	var mapOptions = {
		center : myLatlng,
		zoom : 16,
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("mapLocate"), mapOptions);

	//creo il marker
	var marker = new google.maps.Marker({
			position : new google.maps.LatLng(latit, longit),
			map : map,
			title : "Stazione"
		});

});
