// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'chart.js','ngMaterial', 'starter.services'])


//Configurazione grafici
.config(['ChartJsProvider', function (ChartJsProvider) {
			// Configure all charts
			ChartJsProvider.setOptions({
				responsive : true
			});
		}
	])
	
	//fix doppioclick se si usa angular material
	.config(function( $mdGestureProvider ) {
    $mdGestureProvider.skipClickHijack();
  })

.run(function ($ionicPlatform) {
	$ionicPlatform.ready(function () {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);

		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
	});
})

.config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider

	.state('app', {
		url : '/app',
		abstract : true,
		templateUrl : 'templates/menu.html'
	})

	.state('app.homepage', {
		url : '/homepage',
		views : {
			'menuContent' : {
				templateUrl : 'templates/homepage.html',
				controller : 'homepage'
			}
		}
	})

	.state('app.stationslist', {
		url : '/stationslist',
		views : {
			'menuContent' : {
				templateUrl : 'templates/stationslist.html',
				controller : 'stationsList'
			}
		}
	})

	.state('app.mapstations', {
		url : '/mapstations',
		views : {
			'menuContent' : {
				templateUrl : 'templates/mapstations.html',
				controller : 'stationsMap'
			}
		}
	})

	.state('app.station', {
		url : '/station',
		views : {
			'menuContent' : {
				templateUrl : 'templates/station.html',
				controller : 'stationData'
			}
		}
	})

	.state('app.locatestation', {
		url : '/locatestation',
		views : {
			'menuContent' : {
				templateUrl : 'templates/locatestation.html',
				controller : 'locateStation'
			}
		}
	})


	.state('app.statistics', {
		url : '/statistics',
		views : {
			'menuContent' : {
				templateUrl : 'templates/statistics.html',
				controller : 'statistics'
			}
		}
	})
	
	.state('app.alarms', {
		url : '/alarms',
		views : {
			'menuContent' : {
				templateUrl : 'templates/alarms.html',
				controller : 'alarms'
			}
		}
	});

	

	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise('/app/homepage');
});
