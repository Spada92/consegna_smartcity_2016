angular.module('starter')

//**********PER LE STATISTICHE**************
.controller('statistics', function ($scope, $ionicModal, $ionicLoading, $ionicPopup, stationsList) {

	//dati del server
	var data;
	//item della lista di numeri di stazione del select
	$scope.items = [];

	//per fare gli alert con grafica decente
	$scope.showAlert = function (title, text) {
		var alertPopup = $ionicPopup.alert({
				title : title,
				template : text
			});
	};

	//dice tra due date quella più recente
	function sort(d1, d2) {
		var x = d1 - d2;
		if (x == 0) {
			return 0; //uguali
		}
		if (x > 0) {
			return 1; //d1 maggiore
		}
		return -1; //d2 maggiore
	}

	function takeStations() {
		//mostro lo spinner: inizio a caricare
		$ionicLoading.show({
			templateUrl : 'templates/spinner.html',
			scope : $scope
		});
		//scarico i dati dal server
		stationsList.downloadStationsNumbers().then(function (res) {
			var list = res;
			//check errori
			if (res == 'err') {
				$ionicLoading.hide();
				//evento per il pull down refresh (indica che è terminato)
				$scope.$broadcast('scroll.refreshComplete');
				$scope.showAlert('Errore di rete', 'Impossibile raggiungere il server...controlla la tua connessione e riprova!');
			} else {
				//creo la lista con i dati
				for (var i = 0; i < list.length; i++) {
					var item = ("Stazione " + list[i]);
					$scope.items.push(item);
				}
				//evento per il pull down refresh (indica che è terminato)
				$scope.$broadcast('scroll.refreshComplete');
				//nascondo lo spinner: ho finito di caricare (solo quando viene lanciata la pagina la prima volta, non è del pull down refresh)
				$ionicLoading.hide();
			}
		});
	}

	//li recupero all'avvio della pagina
	takeStations();

	//elementi delle date + other dimension +station number(partono disabilitati)
	var startdate = document.getElementById("startdate");
	var enddate = document.getElementById("enddate");
	startdate.setAttribute("disabled", "disabled");
	enddate.setAttribute("disabled", "disabled");
	var dimension = document.getElementById("dimension");
	dimension.setAttribute("disabled", "disabled");
	var station = document.getElementById("station");
	station.setAttribute("disabled", "disabled");

	//setto la data minima del datepicker
	$scope.startDate = new Date();
	$scope.endDate = new Date();
	$scope.minStart = new Date("Thu Jan 1 2015 GMT-0500 (EST)");
	$scope.minEnd = new Date("Thu Jan 1 2015 GMT-0500 (EST)");

	//valore checkboxs
	$scope.check1 = false;
	$scope.check2 = false;
	$scope.check3 = false;

	//selezione data col datepicker + checkbox
	$scope.changeStartDate = function (newDate) {
		$scope.startDate = newDate;
	};

	$scope.changeEndDate = function (newDate) {
		$scope.endDate = newDate;
	};

	$scope.changeCheck1 = function (item) {
		$scope.check1 = item;
		if ($scope.check1) {
			dimension.removeAttribute("disabled");
		} else {
			dimension.setAttribute("disabled", "disabled");
		}
	};

	$scope.changeCheck2 = function (item) {
		$scope.check2 = item;
		if ($scope.check2) {
			startdate.removeAttribute("disabled");
			enddate.removeAttribute("disabled");
		} else {
			startdate.setAttribute("disabled", "disabled");
			enddate.setAttribute("disabled", "disabled");
		}
	};

	$scope.changeCheck3 = function (item) {
		$scope.check3 = item;
		if ($scope.check3) {
			station.removeAttribute("disabled");
		} else {
			station.setAttribute("disabled", "disabled");
		}
	};

	//tengo salvate le selezioni dei dropdown
	var first = "Temperature";
	var second = "Average";
	var third;

	//salvo quando cambiano le selezioni delle liste
	$scope.saveFirstValue = function (select) {
		first = select;
	};
	$scope.saveSecondValue = function (select) {
		second = select;
	};
	$scope.saveStationNumber = function (select) {
		third = select.split("Stazione ")[1];
	};

	//temperatura
	var tempList = ['<5 °C', '0-10 °C', '11-15 °C', '16-20 °C', '21-25 °C', '26-30 °C', '31-35 °C', '36-40 °C', '41-45 °C', '46-50 °C'];
	//umidità
	var humidList = ['0-10 %', '11-20 %', '21-30 %', '31-40 %', '41-50 %', '51-60 %', '61-70 %', '71-80 %', '81-90 %', '91-100 %'];
	//altitudine
	var altitList = ['<500 m', '501-1000 m', '1001-1500 m', '1501-2000 m', '2001-2500 m', '2501-3000 m', '3001-3500 m', '3501-4000 m', '4001-4500 m', '4501-5000 m'];
	//pioggia
	var rainList = ['Non piove', 'Piove'];
	//mq9
	var mq9List = ['0-120 ppm', '121-240 ppm', '241-360 ppm', '361-480 ppm', '481-600 ppm', '601-720 ppm', '721-840 ppm', '841-960 ppm', '961-1080 ppm', '1081-1200 ppm'];

	//click bottone
	$scope.calculate = function () {
		//controllo che sia stato messo tutto
		if (typeof $scope.startDate == 'object' && typeof $scope.endDate == 'object') {
			//controllo che il secondo mese non sia maggiore del primo
			if ((sort($scope.startDate, $scope.endDate)) == 1) {
				$scope.showAlert('Errore', 'Devi specificare una data finale valida!');
			} else {
				if ($scope.check3) {
					if (typeof third == 'string') {
						doJob();
					} else {
						$scope.showAlert('I dati inseriti non sono validi', 'Devi specificare un numero di stazione!');
					}
				} else {
					doJob();
				}
			}
		} else {
			$scope.showAlert('I dati inseriti non sono validi', 'Devi specificare tutti i dati!');
		}
	};

	//dati grafico
	var canvas = document.getElementById('myChart');
	ctx = canvas.getContext('2d');

	//bottone download
	document.getElementById('download').addEventListener('click', function () {
		downloadCanvas(this, 'myChart', 'chart.png');
	}, false);

	//per capire se mobile o desktop
	function detectmob() {
		if (navigator.userAgent.match(/Android/i)
			 || navigator.userAgent.match(/webOS/i)
			 || navigator.userAgent.match(/iPhone/i)
			 || navigator.userAgent.match(/iPad/i)
			 || navigator.userAgent.match(/iPod/i)
			 || navigator.userAgent.match(/BlackBerry/i)
			 || navigator.userAgent.match(/Windows Phone/i)) {
			return true;
		} else {
			return false;
		}
	}

	//download del canvas come immagine
	function downloadCanvas(link, canvasId, filename) {
		if (detectmob() == true) {
			//se mobile
			window.canvas2ImagePlugin.saveImageDataToLibrary(
				function (msg) {
				$scope.showAlert('Immagine salvata', 'Puoi vedere l\'immagine salvata nella galleria!');
			},
				function (err) {
				$scope.showAlert('Errore', 'Un errore è avvenuto durante il salvataggio dell\'immagine');
			},
				canvas);
		} else {
			//se desktop
			link.href = document.getElementById(canvasId).toDataURL();
			link.download = filename;
		}

	}

	//dai dati faccio il grafico
	function doJob() {

		//mostro lo spinner: inizio a caricare
		$ionicLoading.show({
			templateUrl : 'templates/spinner.html',
			scope : $scope
		});

		//preparo la get al server
		var command;

		//solo dust senza data specificata e senza numero di stazione(gli do semplicemente tutti i dust)
		if ($scope.check1 == false && $scope.check2 == false && $scope.check3 == false) {
			command = "?no&no&no&no";
		}
		//dust con solo numero di stazione ma niente data
		if ($scope.check1 == false && $scope.check2 == false && $scope.check3 == true) {
			var number = third;
			command = "?no&no&no&" + number;
		}
		//dust con solo data ma niente numero di stazione
		if ($scope.check1 == false && $scope.check2 == true && $scope.check3 == false) {
			var d1 = $scope.startDate.toISOString();
			var d2 = $scope.endDate.toISOString();
			command = "?no&" + d1 + "&" + d2 + "&no";
		}
		//dust con data e numero di stazione
		if ($scope.check1 == false && $scope.check2 == true && $scope.check3 == true) {
			var number = third;
			var d1 = $scope.startDate.toISOString();
			var d2 = $scope.endDate.toISOString();
			command = "?no&" + d1 + "&" + d2 + "&" + number;
		}
		//dimensione senza data e senza numero di stazione
		if ($scope.check1 == true && $scope.check2 == false && $scope.check3 == false) {
			var dimension = first;
			command = "?" + dimension + "&no&no&no";
		}
		//dimensione con data ma senza numero di stazione
		if ($scope.check1 == true && $scope.check2 == true && $scope.check3 == false) {
			var dimension = first;
			var d1 = $scope.startDate.toISOString();
			var d2 = $scope.endDate.toISOString();
			command = "?" + dimension + "&" + d1 + "&" + d2 + "&no";
		}
		//dimensione senza data ma con numero di stazione
		if ($scope.check1 == true && $scope.check2 == false && $scope.check3 == true) {
			var dimension = first;
			var number = third;
			command = "?" + dimension + "&no&no&" + number;
		}
		//dimensione con data e con numero di stazione
		if ($scope.check1 == true && $scope.check2 == true && $scope.check3 == true) {
			var dimension = first;
			var number = third;
			var d1 = $scope.startDate.toISOString();
			var d2 = $scope.endDate.toISOString();
			command = "?" + dimension + "&" + d1 + "&" + d2 + "&" + number;
		}

		//scarico i dati dal server
		stationsList.downloadStatisticsData(command).then(function (res) {
			data = res;
			//check errori
			if (res == 'err') {
				$ionicLoading.hide();
				$scope.showAlert('Errore di rete', 'Impossibile raggiungere il server...controlla la tua connessione e riprova!');
			} else {
				var results;
				//non si confronta con le altre dimensioni
				if ($scope.check1 == false) {
					var onlyDust = [[]];
					for (var i = 0; i < data.length; i++) {
						onlyDust[0].push(parseInt(data[i].Dust));
					}
					//calcolo statistica
					results = calcStat(onlyDust);
					//setto nel grafico
					$scope.data = [
						results
					];
					$scope.labels = ["µ/m³"];
				} else {
					//confronto con altre dimensioni
					if (first == "Temperature") {
						var listDust = [[], [], [], [], [], [], [], [], [], []];
						//in base agli intervalli aggiungo negli array
						for (var i = 0; i < data.length; i++) {
							if (parseInt(data[i].Temperature) <= 5) {
								listDust[0].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Temperature) <= 10) {
								listDust[1].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Temperature) <= 15) {
								listDust[2].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Temperature) <= 20) {
								listDust[3].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Temperature) <= 25) {
								listDust[4].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Temperature) <= 30) {
								listDust[5].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Temperature) <= 35) {
								listDust[6].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Temperature) <= 40) {
								listDust[7].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Temperature) <= 45) {
								listDust[8].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Temperature) <= 50) {
								listDust[9].push(parseInt(data[i].Dust));
							}
						}

						//calcolo statistica
						results = calcStat(listDust);

						//setto nel grafico
						$scope.data = [
							results
						];

						$scope.labels = tempList;

					}

					//SE E' SELEZIONATA L'UMIDITA'
					if (first == "Humidity") {
						var listDust = [[], [], [], [], [], [], [], [], [], []];
						for (var i = 0; i < data.length; i++) {
							if (parseInt(data[i].Humidity) <= 10) {
								listDust[0].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Humidity) <= 20) {
								listDust[1].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Humidity) <= 30) {
								listDust[2].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Humidity) <= 40) {
								listDust[3].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Humidity) <= 50) {
								listDust[4].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Humidity) <= 60) {
								listDust[5].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Humidity) <= 70) {
								listDust[6].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Humidity) <= 80) {
								listDust[7].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Humidity) <= 90) {
								listDust[8].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Humidity) <= 100) {
								listDust[9].push(parseInt(data[i].Dust));
							}
						}

						//calcolo statistica
						var results = calcStat(listDust);

						//setto nel grafico
						$scope.data = [
							results
						];
						$scope.labels = humidList;
					}
					//SE E' SELEZIONATA L'ALTITUDINE
					if (first == "GPS_altitude") {
						var listDust = [[], [], [], [], [], [], [], [], [], []];
						for (var i = 0; i < data.length; i++) {
							if (parseInt(data[i].GPS_altitude) <= 500) {
								listDust[0].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].GPS_altitude) <= 1000) {
								listDust[1].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].GPS_altitude) <= 1500) {
								listDust[2].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].GPS_altitude) <= 2000) {
								listDust[3].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].GPS_altitude) <= 2500) {
								listDust[4].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].GPS_altitude) <= 3000) {
								listDust[5].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].GPS_altitude) <= 3500) {
								listDust[6].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].GPS_altitude) <= 4000) {
								listDust[7].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].GPS_altitude) <= 4500) {
								listDust[8].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].GPS_altitude) <= 5000) {
								listDust[9].push(parseInt(data[i].Dust));
							}
						}

						//calcolo statistica
						var results = calcStat(listDust);

						//setto nel grafico
						$scope.data = [
							results
						];
						$scope.labels = altitList;
					}

					//SE E' SELEZIONATO MQ9
					if (first == "MQ9") {
						var listDust = [[], [], [], [], [], [], [], [], [], []];
						for (var i = 0; i < data.length; i++) {
							if (parseInt(data[i].MQ9) <= 120) {
								listDust[0].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].MQ9) <= 240) {
								listDust[1].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].MQ9) <= 360) {
								listDust[2].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].MQ9) <= 480) {
								listDust[3].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].MQ9) <= 600) {
								listDust[4].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].MQ9) <= 720) {
								listDust[5].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].MQ9) <= 840) {
								listDust[6].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].MQ9) <= 960) {
								listDust[7].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].MQ9) <= 1080) {
								listDust[8].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].MQ9) <= 1200) {
								listDust[9].push(parseInt(data[i].Dust));
							}
						}

						//calcolo statistica
						var results = calcStat(listDust);

						//setto nel grafico
						$scope.data = [
							results
						];
						$scope.labels = mq9List;
					}

					//SE E' SELEZIONATA LA PIOGGIA
					if (first == "Rain") {
						var listDust = [[], [], []];
						for (var i = 0; i < data.length; i++) {
							if (parseInt(data[i].Rain) == 0) {
								listDust[0].push(parseInt(data[i].Dust));
							} else if (parseInt(data[i].Rain) == 1) {
								listDust[1].push(parseInt(data[i].Dust));
							}
						}
						//calcolo statistica
						var results = calcStat(listDust);

						//setto nel grafico
						$scope.data = [
							results
						];
						$scope.labels = rainList;
					}

				}

				//nascondo lo spinner: ho finito di caricare (solo quando viene lanciata la pagina la prima volta, non è del pull down refresh)
				$ionicLoading.hide();
			}
		});

	}

	//in base al tipo di statistica scelta faccio il calcolo
	function calcStat(values) {
		var result = [];
		for (var i = 0; i < values.length; i++) {
			result.push(0);
			if (values[i].length == 0) {
				result[i] = 0;
			} else {
				var numbers = new gauss.Vector(values[i]);
				//minimo
				if (second == "Smallest") {
					numbers.min(function (res) {
						result[i] = res;
					});
				}
				//massimo
				if (second == "Bigger") {
					numbers.max(function (res) {
						result[i] = res;
					});
				}
				//range
				if (second == "Range") {
					numbers.range(function (res) {
						result[i] = res;
					});
				}
				//media
				if (second == "Average") {
					numbers.mean(function (res) {
						result[i] = res;
					});
				}
				//mediana
				if (second == "Mean") {
					numbers.median(function (res) {
						result[i] = res;
					});
				}
				//varianza
				if (second == "Variance") {
					numbers.variance(function (res) {
						result[i] = res;
					});
				}
				//deviazione standard
				if (second == "StDeviation") {
					numbers.stdev(null, function (res) {
						result[i] = res;
					});
				}
			}
		}
		return result;
	}
});
