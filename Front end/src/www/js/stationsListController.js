angular.module('starter')

//**********PER LA LISTA DELLE STAZIONI**************
.controller('stationsList', function ($scope, $ionicModal, $ionicLoading, $ionicPopup, stationsList) {
	

	//dati del server
	var data;
	//item della lista
	$scope.items = [];

	//per fare gli alert con grafica decente
	$scope.showAlert = function (title, text) {
		var alertPopup = $ionicPopup.alert({
				title : title,
				template : text
			});
	};

	//onclick degli elementi della lista: carico la pagina coi dati
	$scope.viewStation = function (item) {
		var num = item.split("Stazione ")[1];
		window.location.href = "#/app/station" + "?" + num;
	};

	//refresh dati
	$scope.doRefresh = function () {
		//pulisco la lista
		$scope.items = [];
		//scarico i dati
		stationsList.downloadStationsNumbers().then(function (res) {
			data = res;
			if (res == 'err') {
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
				$scope.showAlert('Errore di rete', 'Impossibile raggiungere il server...controlla la tua connessione e riprova!');
			} else {
				doJob();
			}
		})
	};

	function doJob() {
		//mostro lo spinner: inizio a caricare
		$ionicLoading.show({
			templateUrl : 'templates/spinner.html',
			scope : $scope
		});
		//scarico i dati dal server
		stationsList.downloadStationsNumbers().then(function (res) {
			data = res;
			//check errori
			if (res == 'err') {
				$ionicLoading.hide();
				//evento per il pull down refresh (indica che è terminato)
				$scope.$broadcast('scroll.refreshComplete');
				$scope.showAlert('Errore di rete', 'Impossibile raggiungere il server...controlla la tua connessione e riprova!');
			} else {
				//creo la lista con i dati
				for (var i = 0; i < data.length; i++) {
					var item = "Stazione " + data[i];
					$scope.items.push(item);
				}
				//evento per il pull down refresh (indica che è terminato)
				$scope.$broadcast('scroll.refreshComplete');
				//nascondo lo spinner: ho finito di caricare (solo quando viene lanciata la pagina la prima volta, non è del pull down refresh)
				$ionicLoading.hide();
			}
		});
	}

	//FACCIO IL LAVORO DEL CONTROLLER
	doJob();

});
