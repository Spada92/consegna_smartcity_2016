angular.module('starter')

//**********PER LA LISTA DELLE STAZIONI**************
.controller('alarms', function ($scope, $ionicModal, $ionicLoading, $ionicPopup, stationsList) {

	//soglie dust da superare
	var sogliaAnnuale = 40;
	var sogliaGiornaliera = 50;
	var numDays = 35;
	var count = 0;

	//dati del server
	var data;

	//item della lista
	$scope.items = [];
	$scope.items2 = [];

	//per fare gli alert con grafica decente
	$scope.showAlert = function (title, text) {
		var alertPopup = $ionicPopup.alert({
				title : title,
				template : text
			});
	};

	//tengo salvate le selezioni dei dropdown
	var year;
	var number;

	//salvo quando cambiano le selezioni delle liste
	$scope.saveYear = function (select) {
		year = select;
	};
	$scope.saveStationNumber = function (select) {
		number = select.split("Stazione ")[1];
	};

	function takeValues() {
		//mostro lo spinner: inizio a caricare
		$ionicLoading.show({
			templateUrl : 'templates/spinner.html',
			scope : $scope
		});
		//scarico i dati dal server
		stationsList.downloadStationsNumbers().then(function (res) {
			var list = res;
			//check errori
			if (res == 'err') {
				$ionicLoading.hide();
				$scope.showAlert('Errore di rete', 'Impossibile raggiungere il server...controlla la tua connessione e riprova!');
			} else {
				//creo la lista con i dati
				for (var i = 0; i < list.length; i++) {
					var item = ("Stazione " + list[i]);
					$scope.items2.push(item);
				}
				count++;
				//nascondo lo spinner: ho finito di caricare (solo quando viene lanciata la pagina la prima volta, non è del pull down refresh)
				if (count == 2) {
					$ionicLoading.hide();
				}
			}
		});

		stationsList.downloadYears().then(function (res) {
			var list = res;
			//check errori
			if (res == 'err') {
				$ionicLoading.hide();
				$scope.showAlert('Errore di rete', 'Impossibile raggiungere il server...controlla la tua connessione e riprova!');
			} else {
				//creo la lista con i dati
				for (var i = 0; i < list.length; i++) {
					var item = (list[i]._id);
					$scope.items.push(item);
				}
				count++;
				//nascondo lo spinner: ho finito di caricare (solo quando viene lanciata la pagina la prima volta, non è del pull down refresh)
				if (count == 2) {
					$ionicLoading.hide();
				}
			}
		});
	}

	//riempio i select
	takeValues();

	//click bottone per vedere gli allarmi
	$scope.seeAlarms = function () {
		//controllo che sia stato messo tutto
		if (typeof year == 'string' && typeof number == 'string') {
			doJob();
		} else {
			$scope.showAlert('I dati inseriti non sono validi', 'Devi specificare tutti i dati richiesti!');
		}
	};

	//calcolo gli allarmi
	function doJob() {
		var command = '?' + year + '&' + number;
		//query al server
		stationsList.downloadAlarmsData(command).then(function (res) {
			var list = res;
			//check errori
			if (res == 'err') {
				$ionicLoading.hide();
				$scope.showAlert('Errore di rete', 'Impossibile raggiungere il server...controlla la tua connessione e riprova!');
			} else {
				//calcolo media annuale e quante volte ho superato il giornaliero
				var dayExc = 0;
				var mean = 0;
				var listForStat = [];
				for (var i = 0; i < list.length; i++) {
					listForStat.push(list[i].mean);
					if (list[i].mean > sogliaGiornaliera) {
						dayExc++;
					}
				}
				mean = calcMean(listForStat);
				//scrivo i risultati
				document.getElementById("year").innerHTML = "";
				document.getElementById("day").innerHTML = "";
				document.getElementById("year").innerHTML = "La media di particolato relativa all'anno selezionato è " + mean + " µ/m³. La massima media permessa è " + sogliaAnnuale + " µ/m³";
				var quanto;
				if (dayExc == 1) {
					quanto = " volta";
				} else {
					quanto = " volte";
				}
				document.getElementById("day").innerHTML = "La media giornaliera massima permessa di " + sogliaGiornaliera + " µ/m³ è stata superata " + dayExc + quanto + ". Il massimo numero di volte in cui è permesso superarla è " + numDays;
				//nascondo lo spinner: ho finito di caricare
				$ionicLoading.hide();
			}
		});
	}

	//calcola la media su un vettore
	function calcMean(values) {
		var result = [];
		for (var i = 0; i < values.length; i++) {
			result.push(0);
			if (values[i].length == 0) {
				result[i] = 0;
			} else {
				var numbers = new gauss.Vector(values[i]);
				//media
				numbers.mean(function (res) {
					result[i] = res;
				});
			}
			return result;
		}
	};

});
