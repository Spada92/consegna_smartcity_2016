angular.module('starter.services', [])

//SERVICE PER PRENDERE I DATI DAL DATABASE
.factory('stationsList', function ($http) {
	//oggetto con le cose che mi servono, es funzione per i dati
	var allData;
	var alarmsData;
	var lastStationData;
	var lastStationsData;
	var stationsNumbers;
	var years;
	var statisticsData;
	return {
		//scarico tutti i dati che posso
		downloadAllData : function () {
			return $http.get('http://webservices-smartcityproject.rhcloud.com/air_log'+"?"+(new Date()), {
				cache : true
			}).then(function (resp) {
				allData = resp.data;
				return resp.data;
			}, function error(resp) {
				//ad esempio: non c'è la connessione, o server down
				return 'err';
			});
		},
		//scarico la lista degli allarmi
		downloadAlarmsData : function (command) {
			return $http.get('http://webservices-smartcityproject.rhcloud.com/stationsAlarms'+command+"?"+(new Date()), {
				cache : true
			}).then(function (resp) {
				alarmsData = resp.data;
				return resp.data;
			}, function error(resp) {
				//ad esempio: non c'è la connessione, o server down
				return 'err';
			});
		},
		//scarico gli ultimi dati emessi da tutte le stazioni
		downloadLastStationsData : function () {
			return $http.get('http://webservices-smartcityproject.rhcloud.com/lastStationsData'+"?"+(new Date()), {
				cache : true
			}).then(function (resp) {
				lastStationsData = resp.data;
				return resp.data;
			}, function error(resp) {
				//ad esempio: non c'è la connessione, o server down
				return 'err';
			});
		},
		//scarico i dati di una stazione
		downloadLastStationData : function (num) {		
			return $http.get('http://webservices-smartcityproject.rhcloud.com/lastStationData?'+num+"?"+(new Date()) , {
				cache : true
			}).then(function (resp) {
				lastStationData = resp.data;
				return resp.data;
			}, function error(resp) {
				//ad esempio: non c'è la connessione, o server down
				return 'err';
			});
		},
		//scarico la dei numeri di stazione
		downloadStationsNumbers : function () {
			return $http.get('http://webservices-smartcityproject.rhcloud.com/stationsNumbers'+"?"+(new Date()), {
				cache : true
			}).then(function (resp) {
				stationsNumbers = resp.data;
				return resp.data;
			}, function error(resp) {
				//ad esempio: non c'è la connessione, o server down
				return 'err';
			});
		},
		//scarico gli anni dei dati del database
		downloadYears : function () {
			return $http.get('http://webservices-smartcityproject.rhcloud.com/years'+"?"+(new Date()), {
				cache : true
			}).then(function (resp) {
				years = resp.data;
				return resp.data;
			}, function error(resp) {
				//ad esempio: non c'è la connessione, o server down
				return 'err';
			});
		},
		//scarico i dati per le statistiche
		downloadStatisticsData : function (command) {
			return $http.get('http://webservices-smartcityproject.rhcloud.com/statistics'+command+"?"+(new Date()), {
				cache : true
			}).then(function (resp) {
				statisticsData = resp.data;
				return resp.data;
			}, function error(resp) {
				//ad esempio: non c'è la connessione, o server down
				return 'err';
			});
		}
	}

});
